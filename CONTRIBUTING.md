# Contributing to SatNOGS DB

Thank you for your interest in contributing to SatNOGS!

Please read the `Contribution Guidelines` in [satnogs-db documentation](https://docs.satnogs.org/en/stable/satnogs-db/docs/contribute.html).

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/satnogs/satnogs-db).
